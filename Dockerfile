FROM php:8.3.0-apache-bookworm

WORKDIR /var/www/html 

RUN a2enmod rewrite
COPY . .
