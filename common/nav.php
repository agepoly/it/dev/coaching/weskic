<header>
    <div class="writting-logo-div">
        <video autoplay muted playsinline id="writting-logo">
            <source src="./assets/videos/weskic-writting.mp4" type="video/mp4">
            Your browser does not support the video tag.
        </video>   
    </div>
    <div id="menu" class="color-5">     
        <nav>
            <img class="logo" src="./assets/images/<?php echo $tulia->website->meta->logo; ?>" alt="<?php echo $tulia->website->name; ?>">
            <div>
                <?php
                    $firstLink = reset($tulia->website->navigation);
                    foreach($tulia->website->navigation as $index => $link) {
                        ?><a href="./<?php echo $link->href; ?>" title="<?php echo $link->title; ?>"><?php echo $link->name; ?></a>
                    <?php
                }?>
            </div>
        </nav>
    </div>
    <div class="snowflakes" aria-hidden="true">
	</div>

    <script>
        function createSnowflake() {
			const snowflake = document.createElement('div');
			snowflake.className = 'snowflake';
			document.querySelector('.snowflakes').appendChild(snowflake);

			const startPosition = Math.random() * window.innerWidth;
			const animationDuration = Math.random() * 4 + 4;

			snowflake.style.left = `${startPosition}px`;
			snowflake.style.animationDuration = `${animationDuration}s`;
		}

		async function startSnowfall() {
			for (let i = 0; i < 200; i++) {
				await new Promise(resolve => setTimeout(resolve, 50));
				createSnowflake();
			}
		}

		startSnowfall();
    </script>
</header>