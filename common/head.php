<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="origin" name="referrer">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="alternate" href="<?php echo $tulia->website->ssl ? 'https' : 'http'; ?>://www.<?php echo $tulia->website->domain; ?>" hreflang="<?php echo $tulia->website->lang; ?>"/>
	<link rel="stylesheet" href="./assets/style_<?php echo $tulia->website->cache_version; ?>.css">

	<link href="https://fonts.googleapis.com/css2?family=Overpass&family=Titillium+Web:wght@400;600;700" rel="stylesheet">

	<link rel="apple-touch-icon" sizes="180x180" href="/assets/images/ico/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="./assets/images/weskic-small-logo.png">
	<link rel="manifest" href="../manifest.json">
	<meta name="msapplication-TileColor" content="#c00d0d">
	<meta name="theme-color" content="#c00d0d">
	<script src="https://kit.fontawesome.com/a60d7d21ba.js" crossorigin="anonymous"></script>
