<?php
$tulia = json_decode(file_get_contents('./../manifest.json'));

function getLinkBasedOnDate() {
    // Date limite : 12 décembre 2023 à midi
    $deadline = strtotime('12 December 2023 12:00:00');

    // Date actuelle
    $currentDate = time();

    // Vérification de la condition
    if ($currentDate >= $deadline) {
        // Retourne le lien 1
        return 'https://polyticket.ch/events/weskic_24/home';
    } else {
        // Retourne le lien 2
        return '#';
    }
}
?>