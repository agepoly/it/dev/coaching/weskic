<?php include_once './../common/functions.php'; ?>
<!DOCTYPE html>
<html lang="<?php echo $tulia->website->lang; ?>">
<head>
	<?php include './../common/head.php'; ?>

	<title>Weskic Official Website</title>
	<meta name="description" content="Bienvenu sur le site officiel du weskic édition 2024 ! Tu pourras t'inscrire à ce weekend inoubliable et trouver toutes les informations d'ont tu as besoin !">
	<meta property="og:image" content="/assets/images/screen.jpg">
	<meta property="og:image:type" content="image/jpg">
	<meta property="og:image:width" content="500">
	<meta property="og:image:height" content="500">
	<meta property="og:title" content="Weskic Official Website">
	<meta property="og:type" content="website">
	<meta property="og:url" content="<?php echo $_SERVER['REQUEST_URI']; ?>">
	<meta property="og:description" content="Bienvenu sur le site officiel du weskic édition 2024 ! Tu pourras t'inscrire à ce weekend inoubliable et trouver toutes les informations d'ont tu as besoin !">
	<link rel="canonical" href="<?php echo $_SERVER['REQUEST_URI']; ?>">

</head>
<body>
	<?php include './../common/nav.php';?>
	<div class="vid-background">
		<video autoplay muted loop playsinline id="banner-vid-phone">
			<source src="./assets/videos/ski-phone.mp4" type="video/mp4">
			Your browser does not support the video tag.
		</video>
		<video autoplay muted loop playsinline id="banner-vid">
			<source src="./assets/videos/ski.mp4" type="video/mp4">
			Your browser does not support the video tag.
		</video>
	</div>

	<?php
		$catchPhrasesList = (array)$tulia->catchPhrases;
		$randomKey = array_rand($catchPhrasesList);
		$randomPhrase = $catchPhrasesList[$randomKey];
	?>

	<article>

		<div class="coutdown-container">
			<div class="countdown">
				<div class="text">
					<p class="t-2">Coming Soon...</p>
					<p class="t-3"></p>
				</div>
				<div class="date">
					<div class="n days"><div>0</div><div>Days</div></div>
					<div class="n hours"><div>00</div><div>Hours</div></div>
					<div class="n minutes"><div>00</div><div>Minutes</div></div>
					<div class="n seconds"><div>00</div><div>Seconds</div></div>
				</div>
			</div>
		</div>

		<p class="catchphrase"><?php echo $randomPhrase ?></p>

		<div style="display: flex; justify-content: center; align-items: center";>
			<a id="inscription-link" class="bu-non-active zoom-animation" href="https://polyticket.ch/events/weskic_24/home" target="_blank" disabled>Je m'inscris au Weskic Edition 2024 !</a>
		</div>

		<p class="catchphrase">Si tu es en BA1 en IC, tu pourras t'inscrires dès le 12 décembre à midi 😉, pour les autres, si tu es bloqué sur le lien Polyticket, c'est normal, tu pourras t'inscrire le 13 décembre à midi, alors attends un peu ! Télécharges la charte du weskic <a href="./assets/files/CharteWESKIC.pdf" class="infos-link" download>ici</a>.</p>

		<h2>Nous, le coaching : </h2>
		<img src="./assets/images/contact.jpg" alt="contact-photo" id="contact-photo">
	</article>
	<script>
		const date = document.querySelectorAll('.date .n');

		const timeLeft = () => {
			const now = new Date();

			var annee = 2023;
			var mois = 11; //commence à 0
			var jour = 12;
			var heure = 12;
			var minute = 00;

			const countdownDate = new Date(annee, mois, jour, heure, minute);

			const timeLeft = countdownDate - now;
			
			const seconds = parseInt((timeLeft % (1000 * 60)) / 1000);
			const minutes = parseInt((timeLeft % (1000 * 60 * 60)) / (1000 * 60));
			const hours = parseInt((timeLeft % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
			const days = parseInt(timeLeft / (1000 * 60 * 60 * 24));

			const time = [days, hours, minutes, seconds].map(time => {
				return time < 10 ? `0${time}` : time
			});

			setTime(time);
		}

		const setTime = (time) => {
			date[0].firstChild.innerHTML = time[0];
			date[1].firstChild.innerHTML = time[1];
			console.log(date)
			date[2].firstChild.innerHTML = time[2];
			date[3].firstChild.innerHTML = time[3];
		}

		const inscriptionLink = document.getElementById('inscription-link');

		const disableButtonUntil = new Date('2023-12-12T12:00:00').getTime();

		const updateButtonState = () => {
			const now = new Date().getTime();
			if (now < disableButtonUntil) {
				inscriptionLink.classList.add('bu-non-active');
				inscriptionLink.href = 'javascript:void(0)';
				inscriptionLink.removeAttribute('target');
				inscriptionLink.setAttribute('disabled', 'true');
			} else {
				inscriptionLink.classList.remove('bu-non-active');
				inscriptionLink.setAttribute('target', '_blank');
				inscriptionLink.removeAttribute('disabled');
			}
		};

		updateButtonState();

		setInterval(updateButtonState, 1000);
	</script>
</body>
</html>
