<?php include_once './../common/functions.php'; ?>
<!DOCTYPE html>
<html lang="<?php echo $tulia->website->lang; ?>">
<head>
	<?php include './../common/head.php'; ?>

	<title>Informations - Weskic 2024</title>
	<meta name="description" content="Bienvenue sur le site officiel du Weskic édition 2024. Toutes les infos importantes ici.">
	<meta property="og:image" content="/assets/images/screen.jpg">
	<meta property="og:image:type" content="image/jpg">
	<meta property="og:image:width" content="500">
	<meta property="og:image:height" content="500">
	<meta property="og:title" content="Informations - Weskic 2024">
	<meta property="og:type" content="website">
	<meta property="og:url" content="<?php echo $_SERVER['REQUEST_URI']; ?>">
	<meta property="og:description" content="Bienvenue sur le site officiel du Weskic édition 2024. Toutes les infos importantes ici.">
	<link rel="canonical" href="<?php echo $_SERVER['REQUEST_URI']; ?>">
</head>
<body>
	<?php include './../common/nav.php'; ?>
	<h2 class="h2-centered">WeskIC c'est ...</h2>

	<article>
		<p>... deux chalets<br>... +220 personnes<br>... 30 staffs dont 15 comités<br>... deux soirées<br>... et surtout (un peu) (beaucoup) de ski !</p>
		
		<p>Mais plus encore, le WeskIC est le meilleur moment de l’année pour rencontrer les gens que tu croises en passant dans l’amphi tous les jours et que tu ne connais pas encore, c’est une excellente occasion de vous rapprocher et de vous sentir de plus en plus à l’aise à l’EPFL. Les études supérieures sont souvent qualifiées des meilleurs années de votre vie, faites de ça une réalité !</p>

		<div class="flex-box">
			<div class="vid-background vid-infos">
				<video autoplay muted loop playsinline>
					<source src="../assets/videos/photos.mp4" type="video/mp4">
					Your browser does not support the video tag.
				</video>
			</div>
		</div>

		<h2>FUN</h2>

		<p>Le weekend-ski c’est des soirées de folie, des activités toutes plus funs les unes que les autres et une compétition d’enfer (hehehe pas plus d'informations) !</p>

		<h2>RAMADAN</h2>

		<p>Nos deux comités catering ont a–ssu-rés. Pour ceux qui se lèvent tôt, vous aurez le droit à un plat très calorique (et trèèèès bon c’est une spécialité maghrébine : ) ) qui peut vous faire tenir toute la journée et vous permettre de skier ! De plus on va tous manger après le coucher du soleil ; ) Si vous ne buvez pas d’alcool, nous aurons une muuuuultitude de soft et de mocktails.</p>

		<h2>SKI</h2>

		<p>Lol il faut bien ! Venez skier avec vos potes, faire des courses dans la neige, prendre un chocolat chaud en haut d’une montagne… Trop cool, hein 😉</p>

		<p>Mais tout ça, tu le savais sûrement déjà ! Alors plus sérieusement, tu peux trouver ci-dessous les réponses à tes questions.</p>

		<h2>FAQ</h2>

		<p>Comment se passe le weekend dans les grandes lignes?<br>On part du vendredi 15 au 17 mars. Le vendredi matin on aura tous rendez-vous à l'EPFL où une superbe compagnie de car nous emmènera jusqu'à Evolène, notre destination pour le weekend. Ensuite, tu peux partir skier avec tes potes pour la journée et on se retrouvera le soir pour un après-ski, un repas et une soirée de malade !<br>Le samedi, si t'as pas fait la fête jusqu'à trop tard tu partiras skier tôt après avoir mangé un bon petit dej. On te fournit le repas de midi et c'est parti pour une journée de ski avec tes potes où tu es totalement libre! Ensuite rebelotte, tu rentres, après-ski, manger, soirée et ((très) petit) dodo.<br>Le dimanche, petit dej/brunch puis départ pour un retour en milieu d'aprem à l'EPFL!</p>

		<p>Mais ça à l'air naze ton weekend là... il a quoi de plus qu'un weekend ski en famille?<br>Ah bah ça... Parles-en avec tes coachs ou des personnes qui ont fait le weekend ski l'année dernière et ils sauront te dire ! C'est des sessions de ski mémorables, des soirées à thème fun, des activités surprises qui régalent, des nouvelles rencontres et bien plus encore...</p>

		<p>Qu'est-ce qui est compris dans le prix?<br>TOUT !!! c'est à dire les 2 nuits, à manger pour TOUT le weekend (matin, midi, soir et snack), des activités fun, des soirées de qualité, de la boisson (avec ou sans alcool) illimitée... Il te restera juste à acheter ton abo de ski, comme ça tu décides si tu veux skier un ou deux jours.</p>

		<p>Mais moi je skie pas !!<br>Ah ouais? t'inquiète pas une super team animation t'a préparé des activités pour tout le weekend, manuelles, sportives ou challengeante, il y en aura pour tous les goûts! </p>

		<p>Mais moi j'ai pas de skiiiiiiiis<br>T'inquiète pas non plus ! Notre team sponsoring t'a déniché un super rabais pour louer tout ton matos! Les infos suiveront.</p>

		<p>Mais moi je fais le ramadaaaaaan !! je fais comment pour manger à 5h du matin?<br>Alors ça on l'a aussi prévu ! On a pris très au sérieux cet élément là qui est très important pour beaucoup d'entre nous! Notre team catering s'occupe de tout, vous aurez de quoi manger le suhur à l'heure, et en bonne compagnie 🙂 Important: pour tout.e.s ceux.elles qui feront le ramadan à ce moment-là, il faudra nous l'indiquer dans le formulaire lors de l'inscription !</p>

		<p>Voilà ! Pfiou ! Alors si t'as encore des questions ou si tu veux que je te donne encore plus de raisons de venir, n'hésite pas à me contacter sur telegram (@samaaraaah) ou sur le compte insta du coaching ! (@coaching_ic)</p>

        <h2>Nous, le coaching : </h2>
		<img src="../assets/images/contact.jpg" alt="contact-photo" id="contact-photo">
	</article>
</body>
</html>
