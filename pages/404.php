<?php include_once './../common/functions.php'; ?>
<!DOCTYPE html>
<html lang="<?php echo $tulia->website->lang; ?>">
<head>
	<?php include './../common/head.php'; ?>

	<title>Weskic Official Website</title>
	<meta name="description" content="Bienvenu sur le site officiel du weskic édition 2024 ! Tu pourras t'inscrire à ce weekend inoubliable et trouver toutes les informations d'ont tu as besoin !">
	<meta property="og:image" content="/assets/images/screen.jpg">
	<meta property="og:image:type" content="image/jpg">
	<meta property="og:image:width" content="500">
	<meta property="og:image:height" content="500">
	<meta property="og:title" content="Weskic Official Website">
	<meta property="og:type" content="website">
	<meta property="og:url" content="<?php echo $_SERVER['REQUEST_URI']; ?>">
	<meta property="og:description" content="Bienvenu sur le site officiel du weskic édition 2024 ! Tu pourras t'inscrire à ce weekend inoubliable et trouver toutes les informations d'ont tu as besoin !">
	<link rel="canonical" href="<?php echo $_SERVER['REQUEST_URI']; ?>">

</head>
<body class="model1">
	<?php include './../common/nav.php'; ?>

    <div style="padding: 200px 0; text-align:center">
		<h1>404</h1>
        <p>Sorry, this page do not exist.</p>
    </div>
	
	<?php include './../common/footer.php'; ?>
</body>
</html>